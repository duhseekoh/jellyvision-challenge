import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import Total from '../../src/components/Total';

describe('<Total />', function() {
  it('renders', function() {
    const wrapper = shallow(<Total costs={{}} options={{}} />);
    expect(wrapper.find('.Total')).to.be.lengthOf(1);
  });

  it('renders with no costs initially set', function() {
    const wrapper = shallow(<Total costs={{}} options={{}} />);
    expect(wrapper.find('.calculated-total').text()).to.equal('$0');
  });

  it('renders with the initial price as the base price', function() {
    const wrapper = shallow(<Total costs={{ base: 7.99 }} options={{}} />);
    expect(wrapper.find('.calculated-total').text()).to.equal('$7.99');
  });

  it('should adjust the total when a multiplier is selected', function() {
    const wrapper = shallow(<Total costs={{
      base: 8.00,
      small: 0.5
    }} options={{
      small: {
        label: 'Small Size',
        type: 'multiplier',
      }
    }} />);
    wrapper.instance().handleToggleChange({ target: { checked: true } }, 'small');
    wrapper.update();
    expect(wrapper.find('.calculated-total').text()).to.equal('$4');
  });

  // TODO - normally this logic would be tested outside of React, preferably
  // in reducers and selectors. Opted to test it here, rather than test this
  // logic indirectly via component testing.
  describe('when the oneOnly constraint is applied to a set of toggle fields', function() {
    beforeEach(function() {
      this.wrapper = shallow(<Total costs={{}} options={{
        dummy: {
          constraint: 'oneOnly',
          name: 'dummy',
          label: 'Dummy',
          type: 'multiplier',
        },
        small: {
          constraint: 'oneOnly',
          name: 'size',
          label: 'Small Size',
          type: 'multiplier',
        },
        medium: {
          constraint: 'oneOnly',
          name: 'size',
          label: 'Medium Size',
          type: 'multiplier',
        },
        large: {
          constraint: 'oneOnly',
          name: 'size',
          label: 'Large Size',
          type: 'multiplier',
        },
      }} />);
    });

    it('should update fieldValues when an initial option is selected', function() {
      expect(this.wrapper.instance().state.fieldValues).to.eql({
        dummy: 0,
        small: 0,
        medium: 0,
        large: 0,
      });
      // Simulate option selection
      this.wrapper.instance().handleToggleChange({ target: { checked: true } }, 'small');
      expect(this.wrapper.instance().state.fieldValues).to.eql({
        dummy: 0,
        small: true,
        medium: false,
        large: false,
      });
      // Simulate option deselection
      this.wrapper.instance().handleToggleChange({ target: { checked: false } }, 'small');
      expect(this.wrapper.instance().state.fieldValues).to.eql({
        dummy: 0,
        small: false,
        medium: false,
        large: false,
      });
    });

    it('should deselect fields of same name when field is selected', function() {
      // Set initial size selection
      this.wrapper.instance().handleToggleChange({ target: { checked: true } }, 'small');
      // Set to another size
      this.wrapper.instance().handleToggleChange({ target: { checked: true } }, 'medium');
      expect(this.wrapper.instance().state.fieldValues).to.eql({
        dummy: 0,
        small: false,
        medium: true,
        large: false,
      });
    });
  });
});
