1. Is the feature request clear? Did you feel like you needed more information?
- The base price request is somewhat ambiguous. I originally thought it was adding an additional 'regular' size. Then realized how the large and small multipliers acted upon the full sandwich price. I made the assumption that the user must pick either large or small.
- Not sure what the text "at the 'pricingModel' level" means. I assume it means to be able to configure the price of each sandwich like the other pricing options are configured. But it's odd that 'pricingModel' is quoted and camel cased, like it should already be a variable somewhere in the code.

2. Is there anything you would have done differently if you had more time or resources?
- Listed in 3.

3. How could the code be structured differently to be easier to work with?
- Total.js should be broken up. It's doing a bunch of business logic calculations and responsible for displaying a dynamic UI at the same time.
- options.json should be restructured so the UI can display subtotal, toggle, or multiplier fields wherever is desired. Right now, the way the cost is calculated for a particular field determines where it sits in the UI.
- Hard to test the business logic as it exists. Sure methods like calculateTotal and my new constraint handler could be tested directly on the component (as I did here), alongside the component state, but I prefer to test my components based on their input (props) and output (elements) itself, not the internals.
- Come up with a more robust approach to handle validations.
- Use enums when referencing things like option types (e.g. OPTION_TYPES.MULTIPLIER, OPTION_TYPES.SUBTOTAL)
- Introduce a state management solution (e.g. redux, mobx) or move state management to the root level component. Allows us to keep our state in one location and have our business logic calculations there. It's odd, even for the root level component, to be concerned with navigating a nested json structure to get the price list based on several options selected. Use selectors instead, which are easy to test and entirely outside of React component logic/testing.
- Give brief code doc descriptions of each component, and document what each proptype does.
- Bring in Flow or Typescript, and generate proptypes from those types. (at least use PropTypes.shape instead of PropTypes.object)
- Use prettier to auto-format files, and update eslint rules to play nicely with prettier. Codifying code style rules.
- Use a UI framework with better React integration. Styling centered around components rather than class hierarchy.

4. Is there anything else you think we should know?
- My bugfix solution could be much simpler by hardcoding a check for 'large' and 'small' keys and resetting one another or stopping selection of one when another is selected (or separating out size into a set of radio buttons). I thought it'd be a good idea to start laying the groundwork for a more robust option selection and validation system. Which is why I introduced the concept of a constraint. This also allows for the addition of more sizes (xlarge, xsmall, etc...) by only updating the options.json file.
